using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    private float _timeScaleBeforePause;
    private CanvasController _canvasController;

    private bool _justUnpaused;

    private bool _hasStarted;
    private bool _isPaused;

    private FreqPassToggler _freqPassToggler;

    private void Awake()
    {
        InitializeSingleton();

        SceneManager.activeSceneChanged += (prevScene, curScene) =>
        {
            if (curScene.name == "main")
            {
                _hasStarted = true;
                FindObjectOfType<MusicController>().PlayCurrentClip();
            }
        };
    }

    private void Start()
    {
        _freqPassToggler = FindObjectOfType<FreqPassToggler>();
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            print("Reset scene...");
            LevelManager.Instance.ResetScene();
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            print("Next scene...");
            LevelManager.Instance.LoadNextScene();
        }

        if (!_justUnpaused && _hasStarted && !_isPaused && Input.GetKeyDown(KeyCode.Escape))
            Pause();

        _justUnpaused = false;
    }

    public void Pause()
    {
        _freqPassToggler.TurnOn();

        _timeScaleBeforePause = Time.timeScale;
        MainThreadDispatcher.StartUpdateMicroCoroutine(SetTimeScaleDelayedCoroutine(0,
            _freqPassToggler.TransitionDuration));

        if (_canvasController == null)
            _canvasController = FindObjectOfType<CanvasController>();

        _canvasController.ShowMenu(_canvasController.PauseMenu);

        _isPaused = true;
    }

    public void Unpause()
    {
        Time.timeScale = _timeScaleBeforePause;

        _freqPassToggler.TurnOff();

        _canvasController.HideMenu(_canvasController.PauseMenu);

        _isPaused = false;
        _justUnpaused = true;
    }

    private IEnumerator SetTimeScaleDelayedCoroutine(float timeScale, float delay = 0)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
        {
            yield return null;
        }
        Time.timeScale = timeScale;
    }
}
