using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MessageHolder : ScriptableObject
{
    public string[] Messages;

    [HideInInspector]
    public IncrementalMessage[] IncrementalMessages;

    public Dictionary<string, IncrementalMessage> MessageCollection = new Dictionary<string, IncrementalMessage>();

    private void OnEnable()
    {
        var length = Messages.Length;
        IncrementalMessages = new IncrementalMessage[length];

        for (var i = 0; i < length; i++)
        {
            var message = Messages[i];
            var typingMessage = new IncrementalMessage(message);

            IncrementalMessages[i] = typingMessage;
            MessageCollection[message] = typingMessage;
        }
    }
}
