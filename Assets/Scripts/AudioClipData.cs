﻿using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu]
public class AudioClipData : ScriptableObject
{
    public AudioClip AudioClip;
    public AudioMixerGroup MixerGroup;

    public FloatRange VolumeRange;
    public FloatRange PitchRange;

    public void Play(AudioSource audioSource, float delay = 0)
    {
        audioSource.clip = AudioClip;
        audioSource.outputAudioMixerGroup = MixerGroup;
        audioSource.volume = VolumeRange.Random();
        audioSource.pitch = PitchRange.Random();

        audioSource.PlayDelayed(delay);
    }
}
