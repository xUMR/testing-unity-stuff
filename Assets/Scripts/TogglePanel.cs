﻿using System.Collections;
using Extensions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class TogglePanel : MonoBehaviour
{
    public float AnimationDuration;

    public bool IsHidden
    {
        get { return Mathf.Approximately(_image.fillAmount, 0); }
    }

    private Image _image;
    private bool _fadingIn;
    private bool _fadingOut;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    public void Show()
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(FadeInCoroutine(AnimationDuration));
    }

    public void Hide()
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(FadeOutCoroutine(AnimationDuration));
    }

    private IEnumerator FadeInCoroutine(float duration)
    {
        if (duration.IsNonPositive())
        {
            _image.fillAmount = 1;
            yield break;
        }

        _fadingOut = false;
        _fadingIn = true;

        while (_fadingIn)
        {
            if (Mathf.Approximately(_image.fillAmount, 1))
            {
                break;
            }

            _image.fillAmount += Time.deltaTime / duration;

            yield return null;
        }

        _fadingIn = false;
    }

    public IEnumerator FadeOutCoroutine(float duration)
    {
        if (duration.IsNonPositive())
        {
            _image.fillAmount = 0;
            yield break;
        }

        _fadingIn = false;
        _fadingOut = true;

        while (_fadingOut)
        {
            if (Mathf.Approximately(_image.fillAmount, 0))
            {
                break;
            }

            _image.fillAmount -= Time.deltaTime / duration;

            yield return null;
        }

        _fadingOut = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            if (IsHidden)
                Show();
            else
                Hide();
        }
    }
}
