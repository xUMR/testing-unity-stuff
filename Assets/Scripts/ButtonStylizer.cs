﻿using Extensions;
using UnityEngine;
using UnityEngine.UI;

public class ButtonStylizer : MonoBehaviour
{
    public int WidthPerLetter = 25;

    void Awake()
    {
        var buttons = FindObjectsOfType<Button>();

        for (var i = buttons.Length - 1; i >= 0; i--)
        {
            var rectTransform = buttons[i].GetComponent<RectTransform>();
            var text = buttons[i].GetComponentInChildren<Text>().text;
            rectTransform.sizeDelta = rectTransform.sizeDelta.WithX(text.Length * WidthPerLetter);
        }
    }
}
