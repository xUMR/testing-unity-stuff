﻿using System;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.Audio;

public class AudioSourcePool : ObjectPool<AudioSource>
{
    public readonly Transform Parent;
    private readonly AudioMixerGroup _audioMixerGroup;
    private readonly Action<AudioSource> _onCreate;
    private readonly Action<AudioSource> _onBeforeRent;

    private static readonly Action<AudioSource> EmptyAction = _ => { };

    public AudioSourcePool(Transform parent, Action<AudioSource> onCreate = null,
        Action<AudioSource> onBeforeRent = null)
    {
        Parent = parent;
        _onCreate = onCreate ?? EmptyAction;
        _onBeforeRent = onBeforeRent ?? EmptyAction;
    }

    protected override AudioSource CreateInstance()
    {
        var audioSource = Parent.gameObject.AddComponent<AudioSource>();
        _onCreate(audioSource);

        return audioSource;
    }

    protected override void OnBeforeRent(AudioSource instance)
    {
        base.OnBeforeRent(instance);
        _onBeforeRent(instance);
    }
}
