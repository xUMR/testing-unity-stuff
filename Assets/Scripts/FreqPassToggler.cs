using System;
using System.Collections;
using Extensions;
using UniRx;
using UnityEngine;
using UnityEngine.Audio;

public class FreqPassToggler : MonoBehaviour
{
    [SerializeField] private AudioMixer _mixer;

    [SerializeField] private string _cutoffFreqParamName;

    public float TransitionDuration;

    [MinMax(10, 22000, 0, 100)] public FloatRange Frequencies;

    private float FreqStep
    {
        get { return (Frequencies.Max - Frequencies.Min) / TransitionDuration; }
    }

    private float CutoffFreq
    {
        get
        {
            float freq;
            if (_mixer.GetFloat(_cutoffFreqParamName, out freq))
                return freq;

            throw new ArgumentException();
        }
        set
        {
            if (!_mixer.SetFloat(_cutoffFreqParamName, value))
                throw new ArgumentException();
        }
    }

    public void Toggle()
    {
        if (CutoffFreq.GreaterOrApprox(Frequencies.Max))
            TurnOn();
        else if (CutoffFreq.LessOrApprox(Frequencies.Min))
            TurnOff();
    }

    public void TurnOn()
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(TurnOnCoroutine());
    }

    public void TurnOff()
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(TurnOffCoroutine());
    }

    private IEnumerator TurnOnCoroutine()
    {
        while (CutoffFreq > Frequencies.Min)
        {
            CutoffFreq -= FreqStep * Time.deltaTime;
            yield return null;
        }
        CutoffFreq = Frequencies.Min;
    }

    private IEnumerator TurnOffCoroutine()
    {
        while (CutoffFreq < Frequencies.Max)
        {
            CutoffFreq += FreqStep * Time.deltaTime;
            yield return null;
        }
        CutoffFreq = Frequencies.Max;
    }
}
