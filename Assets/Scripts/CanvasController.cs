using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasController : MonoBehaviour
{
    private Canvas _canvas;

    public GameObject MainMenu;
    public GameObject PauseMenu;
    public GameObject AudioSettings;
    public GameObject VideoSettings;
    public GameObject QuitMenu;

    [NonSerialized] public GameObject PreviousMenu;
    [NonSerialized] public GameObject PreviousPreviousMenu;
    [NonSerialized] public GameObject CurrentMenu;

    private GameObject[] _menus;

    private GameObject ActiveMenu
    {
        get
        {
            for (var i = _menus.Length - 1; i >= 0; i--)
            {
                var menu = _menus[i];
                if (menu == null)
                    continue;

                if (menu.activeSelf)
                    return menu;
            }

            return null;
        }
    }

    private void Awake()
    {
        _canvas = GetComponent<Canvas>();

        DontDestroyOnLoad(gameObject);

        _menus = new[] {MainMenu, PauseMenu, AudioSettings, VideoSettings, QuitMenu};
    }

    public void ShowMenu(GameObject menu)
    {
        if (menu == null) return;
        menu.gameObject.SetActive(true);
    }

    public void HideMenu(GameObject menu)
    {
        if (menu == null) return;

        menu.gameObject.SetActive(false);
    }

    public void HideAllMenus()
    {
        var prevPrevSet = false;
        for (var i = _menus.Length - 1; i >= 0; i--)
        {
            var menu = _menus[i];
            if (menu == null)
                continue;

            if (!prevPrevSet && PreviousMenu != null)
            {
                PreviousPreviousMenu = PreviousMenu;
                prevPrevSet = true;
            }

            if (menu.activeSelf)
                PreviousMenu = menu;

            HideMenu(menu);
        }
    }

    public void ShowPreviousMenu()
    {
        ShowMenu(PreviousPreviousMenu);
    }

    public void Unpause()
    {
        GameManager.Instance.Unpause();
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void PlayAudio(string clipName)
    {
        AudioManager.Instance.Play(clipName);
    }

    private void Escape(GameObject activeMenu)
    {
        if (activeMenu == null) return;

        HideAllMenus();

        if (activeMenu == PauseMenu)
            Unpause();
        else if (activeMenu == MainMenu)
            ShowMenu(QuitMenu);
        else
            ShowPreviousMenu();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Escape(ActiveMenu);
    }
}
