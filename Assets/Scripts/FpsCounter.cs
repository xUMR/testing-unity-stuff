using System;
using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

public class FpsCounter : MonoBehaviour
{
    [SerializeField] [ReadOnlyOnRuntime] private int _updateEverySpecifiedMs = 100;

    public Text Text;
    [MinMax(0, 240, 0, 10)] public FloatRange FpsRangeMedium;

    private ReactiveProperty<float> _fpsProperty;

    public float CurrentFps
    {
        get { return 1 / Time.unscaledDeltaTime; }
    }

    private void Start()
    {
        _fpsProperty = this.UpdateAsObservable()
            .Sample(TimeSpan.FromMilliseconds(_updateEverySpecifiedMs))
            .Select(_ => CurrentFps)
            .ToReactiveProperty();

        _fpsProperty.SubscribeToText(Text, fps => fps.ToString("F0"), FpsColor);
    }

    private void OnDestroy()
    {
        _fpsProperty.Dispose();
    }

    private Color FpsColor(float fps)
    {
        if (fps < FpsRangeMedium.Min) return Color.red;
        if (FpsRangeMedium.ContainsInclusive(fps)) return Color.yellow;
        return Color.green;
    }
}
