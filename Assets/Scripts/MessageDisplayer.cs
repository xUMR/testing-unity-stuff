using System.Collections;
using Extensions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class MessageDisplayer : MonoBehaviour
{
    public Text Text;
    public MessageHolder MessageHolder;
    public float TypingEffectInterval = .03f;
    public float DurationMultiplier = 1.2f;
    public float ZoomEffectMultiplier = .0015f;

    private bool _isBusy;
    private bool _isTypingEffectDone;

    public void Display(IncrementalMessage message)
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(DisplayCoroutine(message));
    }

    public void Display(string message, float duration)
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(DisplayCoroutine(message, duration));
    }

    private IEnumerator DisplayCoroutine(string message, float duration)
    {
        if (_isBusy)
        {
            Debug.LogWarning("display coroutine has been called while busy");
            yield break;
        }

        _isBusy = true;

        Text.CrossFadeAlpha(0, 0, true);

        Text.text = message;
        Text.CrossFadeAlpha(1, 1, true);

        MainThreadDispatcher.StartUpdateMicroCoroutine(ZoomEffectCoroutine());

        var waitUntil = Time.time + duration + 1;
        while (Time.time < waitUntil)
            yield return null;

        Text.CrossFadeAlpha(0, 1, true);

        waitUntil = Time.time + 1;
        while (Time.time < waitUntil)
            yield return null;

        Text.rectTransform.localScale = Vector3.one;
        Text.text = "";

        _isBusy = false;
    }

    private IEnumerator DisplayCoroutine(IncrementalMessage message)
    {
        if (_isBusy)
        {
            Debug.LogWarning("display coroutine has been called while busy");
            yield break;
        }

        _isBusy = true;
        _isTypingEffectDone = false;

        Text.CrossFadeAlpha(0, 0, true);

        MainThreadDispatcher.StartUpdateMicroCoroutine(TypeEffectCoroutine(message));

        var duration = TypingEffectInterval * message.Length;
        Text.CrossFadeAlpha(1, duration, true);

        MainThreadDispatcher.StartUpdateMicroCoroutine(ZoomEffectCoroutine());

        while (!_isTypingEffectDone)
            yield return null;

        var waitUntil = Time.time + duration * (DurationMultiplier - 1);
        while (Time.time < waitUntil)
            yield return null;

        Text.CrossFadeAlpha(0, 1, true);

        waitUntil = Time.time + 1;
        while (Time.time < waitUntil)
            yield return null;

        Text.rectTransform.localScale = Vector3.one;
        Text.text = "";

        _isBusy = false;
    }

    private IEnumerator ZoomEffectCoroutine()
    {
        while (_isBusy)
        {
            Text.rectTransform.localScale += Vector3.one * ZoomEffectMultiplier;
            yield return null;
        }
    }

    private IEnumerator TypeEffectCoroutine(IncrementalMessage message)
    {
        var length = message.Length;
        for (var i = 0; i < length; i++)
        {
            Text.text = message[i];

            var waitUntil = Time.time + TypingEffectInterval;
            while (Time.time < waitUntil)
                yield return null;
        }
        _isTypingEffectDone = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
//            Display(MessageHolder.IncrementalMessages.RandomElement());
            Display(MessageHolder.IncrementalMessages.RandomElement().Message, 2f);
        }
    }
}
