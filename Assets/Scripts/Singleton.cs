using System;
using Extensions;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance != null)
                return _instance;

            var instances = FindObjectsOfType<T>();

            if (instances.Length > 1)
                throw new Exception(string.Format("Multiple instances of singleton: {0}", typeof(T)));

            GameObject gameObject;
            if (instances.Length == 1)
            {
                _instance = instances[0];
                gameObject = _instance.gameObject;
            }
            else
            {
                gameObject = new GameObject(typeof(T).ToString());
                _instance = gameObject.GetOrAddComponent<T>();
            }

            DontDestroyOnLoad(gameObject);
            if (!gameObject.name.EndsWith(" (Singleton)"))
            {
                gameObject.name = string.Format("{0} (Singleton)", gameObject.name);
            }

            return _instance;
        }
    }

    public static void InitializeSingleton()
    {
        PurgeOtherInstances();
    }

    public static void PurgeOtherInstances()
    {
        if (Instance == null)
            return;

        var instances = FindObjectsOfType<T>();
        if (instances.Length == 1)
            return;

        for (var i = instances.Length - 1; i >= 0; i--)
        {
            var other = instances[i];
            if (other != _instance)
            {
                Destroy(other.gameObject);
            }
        }
    }
}
