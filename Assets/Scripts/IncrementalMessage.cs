﻿using System;
using UnityEngine;

[Serializable]
public class IncrementalMessage
{
    [ReadOnly] public readonly string Message;

    [HideInInspector] public readonly string[] Strings;

    [HideInInspector] public readonly int Length;

    public string this[int i]
    {
        get { return Strings[i]; }
    }

    public IncrementalMessage(string message)
    {
        Message = message;
        Length = message.Length;
        Strings = new string[Length];

        for (var i = 0; i < Length; i++)
        {
            Strings[i] = message.Substring(0, i + 1);
        }
    }
}
