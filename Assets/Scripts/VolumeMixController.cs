using System;
using System.Collections;
using Extensions;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu]
public class VolumeMixController : ScriptableObject
{
    [SerializeField] private AudioMixer _mixer;

    [SerializeField] private string _masterVolumeParamName;
    [SerializeField] private string _musicVolumeParamName;
    [SerializeField] private string _sfxVolumeParamName;
    [SerializeField] private string _uiVolumeParamName;

    private const float RoundingMultiple = .01f;
    public readonly float DefaultVolumeDb = 0;

    #region volume fields & properties

    public int MusicVolume100Int
    {
        get { return Convert.ToInt32(MusicVolume * 100); }
    }

    public int SfxVolume100Int
    {
        get { return Convert.ToInt32(SfxVolume * 100); }
    }

    public int UiVolume100Int
    {
        get { return Convert.ToInt32(UiVolume * 100); }
    }

    public float MasterVolume
    {
        get
        {
            return DbToLinear(MasterVolumeDecibel);
        }
        set
        {
            var clamped = Mathf.Clamp01(value).RoundToNearestMultipleOf(RoundingMultiple);
            MasterVolumeDecibel = LinearToDb(clamped);
        }
    }

    public float MusicVolume
    {
        get
        {
            return DbToLinear(MusicVolumeDecibel);
        }
        set
        {
            var clamped = Mathf.Clamp01(value).RoundToNearestMultipleOf(RoundingMultiple);
            MusicVolumeDecibel = LinearToDb(clamped);
        }
    }

    public float SfxVolume
    {
        get { return DbToLinear(SfxVolumeDecibel); }
        set
        {
            var clamped = Mathf.Clamp01(value).RoundToNearestMultipleOf(RoundingMultiple);
            SfxVolumeDecibel = LinearToDb(clamped);
        }
    }

    public float UiVolume
    {
        get { return DbToLinear(UiVolumeDecibel); }
        set
        {
            var clamped = Mathf.Clamp01(value).RoundToNearestMultipleOf(RoundingMultiple);
            UiVolumeDecibel = LinearToDb(clamped);
        }
    }

    private float MasterVolumeDecibel
    {
        get
        {
            float freq;
            if (_mixer.GetFloat(_masterVolumeParamName, out freq))
                return freq;

            throw new ArgumentException();
        }
        set
        {
            if (!_mixer.SetFloat(_masterVolumeParamName, value))
                throw new ArgumentException();
        }
    }

    private float MusicVolumeDecibel
    {
        get
        {
            float freq;
            if (_mixer.GetFloat(_musicVolumeParamName, out freq))
                return freq;

            throw new ArgumentException();
        }
        set
        {
            if (!_mixer.SetFloat(_musicVolumeParamName, value))
                throw new ArgumentException();
        }
    }


    private float SfxVolumeDecibel
    {
        get
        {
            float freq;
            if (_mixer.GetFloat(_sfxVolumeParamName, out freq))
                return freq;

            throw new ArgumentException();
        }
        set
        {
            if (!_mixer.SetFloat(_sfxVolumeParamName, value))
                throw new ArgumentException();
        }
    }

    private float UiVolumeDecibel
    {
        get
        {
            float freq;
            if (_mixer.GetFloat(_uiVolumeParamName, out freq))
                return freq;

            throw new ArgumentException();
        }
        set
        {
            if (!_mixer.SetFloat(_uiVolumeParamName, value))
                throw new ArgumentException();
        }
    }

    #endregion

    public void ResetVolume()
    {
        MasterVolume = DefaultVolumeDb;
        MusicVolume = DefaultVolumeDb;
        SfxVolume = DefaultVolumeDb;
        UiVolume = DefaultVolumeDb;
    }

    public static float DbToLinear(float db)
    {
        return Mathf.Pow(10, db / 20);
    }

    public static float LinearToDb(float linear)
    {
        if (Mathf.Approximately(linear, 0))
            linear = float.Epsilon;
        return Mathf.Log10(linear) * 20;
    }
}
