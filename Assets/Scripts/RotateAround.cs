﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    public bool IsRoot;
    public float RotationSpeed;
    private Transform _parent;

    // Use this for initialization
    private void Start()
    {
        if (!IsRoot)
            _parent = transform.parent;
    }

    // Update is called once per frame
    void Update()
    {
        var angle = RotationSpeed * Time.deltaTime;

        RotateAroundItself(angle);
        RotateAroundParent(angle);
    }

    private void RotateAroundItself(float angle)
    {
        transform.Rotate(transform.up, angle);
    }

    private void RotateAroundParent(float angle)
    {
        if (!IsRoot)
            transform.RotateAround(_parent.position, _parent.up, angle);
    }
}
