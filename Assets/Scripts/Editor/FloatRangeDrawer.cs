using System;
using System.Linq;
using System.Reflection;
using Extensions;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomPropertyDrawer(typeof(FloatRange))]
    public class FloatRangeDrawer : PropertyDrawer
    {
        // todo display error message if minValue > maxValue

        private readonly GUIContent[] _subLabels = {GUIContent.none, GUIContent.none};

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var minProperty = property.FindPropertyRelative("Min");
            var maxProperty = property.FindPropertyRelative("Max");

            var minValue = minProperty.floatValue;
            var maxValue = maxProperty.floatValue;

            var minAllowed = float.MinValue;
            var maxAllowed = float.MaxValue;

            float step;
            int decimalPlaces;

            EditorGUI.BeginChangeCheck();

            if (HasRangeAttribute(ref minAllowed, ref maxAllowed, out step, out decimalPlaces))
            {
                var sliderLabel = CreateSliderLabel(label.text, minValue, maxValue, decimalPlaces);
                EditorGUI.MinMaxSlider(position, sliderLabel, ref minValue, ref maxValue, minAllowed, maxAllowed);

                if (minValue > minAllowed)
                    minValue = minValue.RoundToNearestMultipleOf(step);
                if (maxValue < maxAllowed)
                    maxValue = maxValue.RoundToNearestMultipleOf(step);
            }
            else
            {
                position = EditorGUI.PrefixLabel(position, label);

                float[] values = {minValue, maxValue};
                EditorGUI.MultiFloatField(position, _subLabels, values);

                minValue = values[0];
                maxValue = values[1];
            }

            if (EditorGUI.EndChangeCheck())
            {
                minProperty.floatValue = minValue;
                maxProperty.floatValue = maxValue;
            }

            EditorGUI.EndProperty();
        }

        private bool HasRangeAttribute(ref float minAllowed, ref float maxAllowed, out float step,
            out int decimalPlaces)
        {
            var minMaxAttribute = Attribute.GetCustomAttribute(fieldInfo, typeof(MinMaxAttribute)) as MinMaxAttribute;

            step = float.Epsilon;
            decimalPlaces = 0;

            if (minMaxAttribute == null)
                return false;

            minAllowed = minMaxAttribute.Min;
            maxAllowed = minMaxAttribute.Max;
            step = minMaxAttribute.Step;
            decimalPlaces = minMaxAttribute.DecimalPlaces;

            return true;
        }

        private string CreateSliderLabel(string label, float min, float max, int decimalPlaces = 0)
        {
            var numberFormat = string.Format("F{0}", decimalPlaces);
            return string.Format("{0} ({1} - {2})", label, min.ToString(numberFormat), max.ToString(numberFormat));
        }
    }
}
