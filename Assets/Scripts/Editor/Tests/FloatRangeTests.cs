﻿using System;
using System.Linq;
using Extensions;
using NUnit.Framework;
using UnityEngine;

namespace Editor
{
    [TestFixture]
    [Timeout(1000)]
    public class FloatRangeTests
    {
        [Test]
        public void TestFoo()
        {
            Debug.Log(72f.RoundToNearestMultipleOf(10));
            Debug.Log(76f.RoundToNearestMultipleOf(10));
            Debug.Log(1.1f.RoundToNearestMultipleOf(.25f));
            Debug.Log(1.15f.RoundToNearestMultipleOf(.25f));
            Debug.Log(1.35f.RoundToNearestMultipleOf(.25f));
            Debug.Log(1.55f.RoundToNearestMultipleOf(.25f));
        }

        [Test]
        public void TestAdd()
        {
            // todo exception cases

            Assert.AreEqual(FloatRange.ZeroOne + new FloatRange(1, 3), FloatRange.Of(0, 3));
            Assert.AreEqual(FloatRange.ZeroOne + new FloatRange(0, 3), FloatRange.Of(0, 3));
            Assert.AreEqual(FloatRange.ZeroOne + new FloatRange(0, 1), FloatRange.ZeroOne);

            Assert.AreEqual(FloatRange.ZeroOne + 1, FloatRange.Of(1, 2));
            Assert.AreEqual(1 + FloatRange.ZeroOne, FloatRange.Of(1, 2));
        }

        [Test]
        public void TestEnumerate()
        {
            Assert.Throws<ArgumentException>(() => FloatRange.Of(2, 1));
            Assert.Throws<ArgumentException>(() => FloatRange.Of(1, 2).AsEnumerable(0));

            Assert.AreEqual(FloatRange.Of(10).AsEnumerable(1, true).Skip(10).First(), 10);
            Assert.AreEqual(FloatRange.Of(10).AsEnumerable().IsInclusive(true)[10], 10);
            Assert.AreEqual(FloatRange.Of(10)[1], 10);

            Assert.AreEqual(FloatRange.Of(10).AsEnumerable().Sum(), 45);
            Assert.AreEqual(FloatRange.Of(10).AsEnumerable(1, true).Sum(), 55);
            Assert.AreEqual(FloatRange.Of(10).AsEnumerable().Count(), 10);
            Assert.AreEqual(FloatRange.Of(10).AsEnumerable(1, true).Count(), 11);

            Assert.AreEqual(FloatRange.Of(10).AsEnumerable(-1).Sum(), 55);

            Assert.AreEqual(FloatRange.Of(10).AsEnumerable().First(), 0);
            Assert.AreEqual(FloatRange.Of(10).AsEnumerable(-1).First(), 10);
        }

        [Test]
        public void TestLength()
        {
            Assert.AreEqual(FloatRange.Of(10).AsEnumerable().Length, 10);
            Assert.AreEqual(FloatRange.Of(10).AsEnumerable().IsInclusive(true).Length, 11);
        }
    }
}
