﻿using NUnit.Framework;
using UnityEngine;

namespace Editor
{
    [TestFixture]
    public class TypingMessageTests
    {
        [Test]
        public void Test()
        {
            var message = new IncrementalMessage("Hello, World!");
            foreach (var s in message.Strings)
            {
                Debug.Log(s);
            }
        }
    }
}
