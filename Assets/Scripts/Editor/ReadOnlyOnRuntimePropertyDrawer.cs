using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ReadOnlyOnRuntimeAttribute))]
public class ReadOnlyOnRuntimePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (Application.isPlaying)
            GUI.enabled = false;

        EditorGUI.PropertyField(position, property, label);

        if (Application.isPlaying)
            GUI.enabled = true;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return Application.isPlaying
            ? EditorGUI.GetPropertyHeight(property, label, true)
            : base.GetPropertyHeight(property, label);
    }
}
