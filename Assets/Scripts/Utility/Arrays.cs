﻿namespace Utility
{
    public static class Arrays<T>
    {
        public static readonly T[] Empty = new T[0];
        public static readonly T[] Single = new T[1];
    }
}
