using System.Collections;
using UnityEngine;

namespace Utility
{
    public static class Wait
    {
        public static readonly WaitForEndOfFrame ForEndOfFrame = new WaitForEndOfFrame();
        public static readonly WaitForFixedUpdate ForFixedUpdate = new WaitForFixedUpdate();

        public static readonly WaitForSeconds ForSeconds001 = new WaitForSeconds(.01f);
        public static readonly WaitForSeconds ForSeconds0025 = new WaitForSeconds(.025f);
        public static readonly WaitForSeconds ForSeconds005 = new WaitForSeconds(.05f);
        public static readonly WaitForSeconds ForSeconds0075 = new WaitForSeconds(.075f);

        public static readonly WaitForSeconds ForSeconds01 = new WaitForSeconds(.1f);
        public static readonly WaitForSeconds ForSeconds025 = new WaitForSeconds(.25f);
        public static readonly WaitForSeconds ForSeconds05 = new WaitForSeconds(.5f);
        public static readonly WaitForSeconds ForSeconds075 = new WaitForSeconds(.75f);

        public static readonly WaitForSeconds ForSeconds1 = new WaitForSeconds(1);
        public static readonly WaitForSeconds ForSeconds10 = new WaitForSeconds(10);

        public static readonly WaitForSecondsRealtime ForSecondsRealtime001 = new WaitForSecondsRealtime(.01f);
        public static readonly WaitForSecondsRealtime ForSecondsRealtime01 = new WaitForSecondsRealtime(.1f);
        public static readonly WaitForSecondsRealtime ForSecondsRealtime1 = new WaitForSecondsRealtime(1);
        public static readonly WaitForSecondsRealtime ForSecondsRealtime10 = new WaitForSecondsRealtime(10);

        // yield return Wait.Coroutine(duration);
        public static IEnumerator Coroutine(float duration)
        {
            var endTime = Time.time + duration;
            while (Time.time < endTime)
                yield return ForEndOfFrame;
        }

        // yield return Wait.CoroutineNull(duration);
        public static IEnumerator CoroutineNull(float duration)
        {
            var endTime = Time.time + duration;
            while (Time.time < endTime)
                yield return null;
        }
    }
}
