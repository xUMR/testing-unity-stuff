using System;
using System.Collections;
using Extensions;
using UniRx;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public Transform AudioSourcePoolParent;
    public AudioSourcePool AudioSourcePool;

    public AudioClipData[] AudioClipData;
    private AudioSource[] _audioSources;

    private void Awake()
    {
        InitializeSingleton();

        Action<AudioSource> onBeforeRent = a =>
        {
            a.playOnAwake = false;
            a.loop = false;
            a.clip = null;
        };

        AudioSourcePool = new AudioSourcePool(AudioSourcePoolParent, onBeforeRent: onBeforeRent);
    }

    public void Play(string clipName, float delay = 0, bool returnInstance = true)
    {
        var audioSource = AudioSourcePool.Rent();
        Find(clipName).Play(audioSource, delay);

        if (returnInstance)
        {
            MainThreadDispatcher.StartUpdateMicroCoroutine(ReturnAudioSourceCoroutine(audioSource, delay));
        }
    }

    private IEnumerator ReturnAudioSourceCoroutine(AudioSource audioSource, float delay = 0)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
            yield return null;

        while (audioSource.isPlaying)
            yield return null;

        AudioSourcePool.Return(audioSource);
    }

    private AudioClipData Find(string clipName)
    {
        for (var i = AudioClipData.Length - 1; i >= 0; i--)
            if (AudioClipData[i].AudioClip.name == clipName)
                return AudioClipData[i];

        throw new ArgumentException();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            GetComponentInChildren<FreqPassToggler>().Toggle();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            var musicController = GetComponentInChildren<MusicController>();
            if (musicController.CurrentAudioSource.isPlaying)
            {
                print("next clip");
                musicController.NextClip();
            }
            else
            {
                print("play");
                musicController.PlayClip(musicController.CurrentAudioClip);
            }
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            MainThreadDispatcher.StartUpdateMicroCoroutine(Camera.main.ShakeCoroutine(5, 0.1f));
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            var musicController = GetComponentInChildren<MusicController>();
            if (musicController.MusicPitch.Approx(.75f))
                musicController.PitchShift(1, 1);
            else
                musicController.PitchShift(.75f, 1);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            var musicController = GetComponentInChildren<MusicController>();
            musicController.PlayClipMatchEnd(musicController.AdditionalClips[0]);
        }
    }
}
