using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : Singleton<LevelManager>
{
    [ReadOnly] [SerializeField] private int _currentSceneIndex;

    [ReadOnly] public string[] Scenes;

#if UNITY_EDITOR
    public SceneAsset[] SceneAssets;
#endif

    private void Awake()
    {
        InitializeSingleton();

        print("SceneManager.sceneCount " + SceneManager.sceneCount);
        print("SceneManager.sceneCountInBuildSettings " + SceneManager.sceneCountInBuildSettings);

//        print(SceneManager.sceneCount);
//        print(SceneManager.sceneCountInBuildSettings);
//        Scenes = new Scene[SceneManager.sceneCount];
//
//        for (var i = 0; i < SceneManager.sceneCount; i++)
//        {
//            Scenes[i] = SceneManager.GetSceneAt(i);
//        }
    }

    private void OnValidate()
    {
#if UNITY_EDITOR
        // https://twitter.com/roboryantron/status/946632721090142208
        if (SceneAssets == null)
            return;

        Scenes = new string[SceneAssets.Length];
        for (var i = SceneAssets.Length - 1; i >= 0; i--)
        {
            var scene = SceneAssets[i];
            Scenes[i] = scene == null ? "" : scene.name;
        }
#endif
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(_currentSceneIndex);
    }

    public void LoadNextScene()
    {
//        SceneManager.LoadScene("menu");
//        return;
//        if (_currentSceneIndex == Scenes.Length - 1)
//        {
//            print("There is no next scene.");
//            return;
//        }
//
//        var nextScene = Scenes[++_currentSceneIndex];
//        SceneManager.LoadScene(nextScene);
    }

    public void LoadPreviousScene()
    {
//        if (_currentSceneIndex == 0)
//        {
//            print("There is no previous scene.");
//            return;
//        }
//
//        var prevScene = Scenes[--_currentSceneIndex];
//        SceneManager.LoadScene(prevScene);
    }
}
