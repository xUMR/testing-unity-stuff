﻿using UnityEngine;

namespace Extensions
{
    public static class GameObjectExtensions
    {
        public static T GetOrAddComponent<T>(this GameObject self) where T : Component
        {
            var component = self.GetComponent<T>();
            return component == null ? self.AddComponent<T>() : component;
        }
    }
}
