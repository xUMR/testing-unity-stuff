using System.Collections;
using UnityEngine;

public static class CameraExtensions
{
    public static IEnumerator ShakeCoroutine(this Camera self, float duration, float strength = 1,
        float resetDuration = 1)
    {
        var transform = self.transform;
        var initialPosition = transform.position;
        var endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            var v = FloatRange.MinusOneOne.RandomVector3() * strength;
            transform.Translate(v);

            yield return null;
        }

        LeanTween.move(self.gameObject, initialPosition, resetDuration).setEase(LeanTweenType.easeOutBack);
    }
}
