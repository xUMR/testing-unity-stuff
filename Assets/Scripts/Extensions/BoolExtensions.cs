﻿namespace Extensions
{
    public static class BoolExtensions
    {
        public static int AsInt(this bool b, int whenTrue = 1, int whenFalse = 0)
        {
            return b ? whenTrue : whenFalse;
        }
    }
}
