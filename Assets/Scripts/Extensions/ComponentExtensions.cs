﻿using UnityEngine;

namespace Extensions
{
    public static class ComponentExtensions
    {
        public static T GetOrAddComponent<T>(this Component self) where T : Component
        {
            var component = self.GetComponent<T>();
            return component == null ? self.gameObject.AddComponent<T>() : component;
        }
    }
}
