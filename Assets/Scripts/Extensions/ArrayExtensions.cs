﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

namespace Extensions
{
    public static class ArrayExtensions
    {
        public static T RandomElement<T>([NotNull] this T[] array)
        {
            if (array.Length == 0)
                throw new ArgumentException("Collection must not be empty.");

            return array.ElementAt(UnityEngine.Random.Range(0, array.Length));
        }

        public static bool Contains<T>([NotNull] this T[] array, T t)
        {
            for (var i = array.Length - 1; i >= 0; i--)
            {
                if (array[i].Equals(t))
                {
                    return true;
                }
            }

            return false;
        }

        public static string AsString<T>(this T[] array)
        {
            var sb = new StringBuilder("[");

            for (var i = 0; i < array.Length - 1; i++)
            {
                sb.Append(array[i]);
                sb.Append(", ");
            }

            sb.Append(array[array.Length - 1]);
            sb.Append("]");

            return sb.ToString();
        }
    }
}
