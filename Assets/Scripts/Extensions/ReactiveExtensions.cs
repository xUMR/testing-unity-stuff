﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Extensions
{
    public static class ReactiveExtensions
    {
        public static IDisposable SubscribeToText<T>(this UniRx.IObservable<T> self, Text text,
            Func<T, string> selector, Func<T, Color> colorSelector)
        {
            return self.SubscribeWithState3(text, selector, colorSelector, (x, t, s, c) =>
            {
                t.color = c(x);
                t.text = s(x);
            });
        }
    }
}
