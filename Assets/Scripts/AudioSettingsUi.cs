using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class AudioSettingsUi : MonoBehaviour
{
    public VolumeMixController VolumeMixController;

    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _sfxSlider;
    [SerializeField] private Slider _uiSlider;

    [SerializeField] private Text _musicValue;
    [SerializeField] private Text _sfxValue;
    [SerializeField] private Text _uiValue;

    private ReactiveProperty<float> _musicProperty;
    private ReactiveProperty<float> _sfxProperty;
    private ReactiveProperty<float> _uiProperty;

    private void Awake()
    {
        _musicSlider.value = VolumeMixController.MusicVolume;
        _sfxSlider.value = VolumeMixController.SfxVolume;
        _uiSlider.value = VolumeMixController.UiVolume;

        _musicSlider.OnValueChangedAsObservable().Subscribe(UpdateMusicVolume);
        _sfxSlider.OnValueChangedAsObservable().Subscribe(UpdateSfxVolume);
        _uiSlider.OnValueChangedAsObservable().Subscribe(UpdateUiVolume);
    }

    private void UpdateMusicVolume(float volume)
    {
        VolumeMixController.MusicVolume = volume;
        _musicValue.text = VolumeMixController.MusicVolume100Int.ToString();
    }

    private void UpdateSfxVolume(float volume)
    {
        VolumeMixController.SfxVolume = volume;
        _sfxValue.text = VolumeMixController.SfxVolume100Int.ToString();
    }

    private void UpdateUiVolume(float volume)
    {
        VolumeMixController.UiVolume = volume;
        _uiValue.text = VolumeMixController.UiVolume100Int.ToString();
    }
}
