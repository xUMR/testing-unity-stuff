using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSkip : MonoBehaviour
{
    public string NextScene;
    private bool _skipped;

    private void LateUpdate()
    {
        if (!_skipped)
        {
            SceneManager.LoadScene(NextScene);
            _skipped = true;
        }
    }
}
