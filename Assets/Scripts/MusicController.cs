using System;
using System.Collections;
using UniRx;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.Audio;

public class MusicController : MonoBehaviour
{
    // todo clip change, compensate for the pitch shift

    public Transform AudioSourcePoolTransform;

    private AudioMixer _mixer;
    public AudioMixerGroup MusicMixerGroup;
    public string MusicPitchParamName;

    public AudioClip[] MusicClips;
    public AudioClip[] AdditionalClips;
    private AudioSource[] _audioSources;
    private bool[] _audioSourceIsBusy;
    private int _currentClipIndex;

    private bool _clipIsChanging;

    public AudioClip CurrentAudioClip
    {
        get { return MusicClips[_currentClipIndex]; }
    }

    public AudioSource CurrentAudioSource
    {
        get { return _audioSources[_currentClipIndex]; }
    }

    private AudioSource NextAudioSource
    {
        get { return _audioSources[(_currentClipIndex + 1) % _audioSources.Length]; }
    }

    public float MusicPitch
    {
        get
        {
            float pitch;
            _mixer.GetFloat(MusicPitchParamName, out pitch);
            return pitch;
        }
        private set
        {
            if (!_mixer.SetFloat(MusicPitchParamName, value))
                throw new ArgumentException();
        }
    }

    private ObjectPool<AudioSource> _additionalAudioSources;

    private void Awake()
    {
        var length = MusicClips.Length;
        _audioSources = new AudioSource[length];
        _audioSourceIsBusy = new bool[length];

        for (var i = 0; i < length; i++)
        {
            var audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            audioSource.loop = true;
            audioSource.clip = MusicClips[i];
            audioSource.outputAudioMixerGroup = MusicMixerGroup;
            _audioSources[i] = audioSource;
        }

        if (AudioSourcePoolTransform == null)
        {
            var go = new GameObject("AudioSourcePool");
            AudioSourcePoolTransform = go.transform;
            AudioSourcePoolTransform.SetParent(transform);
        }

        Action<AudioSource> onBeforeRent = a =>
        {
            a.outputAudioMixerGroup = MusicMixerGroup;
            a.playOnAwake = false;
            a.loop = false;
            a.clip = null;
        };

        _additionalAudioSources = new AudioSourcePool(AudioSourcePoolTransform, onBeforeRent: onBeforeRent);

        _mixer = MusicMixerGroup.audioMixer;
    }

    public void PlayClip(AudioClip clip, float fadeInDuration = 0)
    {
        var audioSource = AudioSourceFor(clip);

        FadeIn(audioSource, fadeInDuration);
    }

    public void PlayCurrentClip(float fadeInDuration = 0)
    {
        PlayClip(CurrentAudioClip, fadeInDuration);
    }

    public void PlayClipMatchEnd(AudioClip clip, float fadeInDuration = 0)
    {
        var mainAudioSource = CurrentAudioSource;
        var mainClip = CurrentAudioClip;

        var audioSource = _additionalAudioSources.Rent();
        audioSource.clip = clip;

        var clipDelay = mainClip.length - mainAudioSource.time - clip.length;
        FadeIn(audioSource, fadeInDuration, clipDelay, false);

        var returnDelay = clipDelay + clip.length;
        ReturnAudioSource(audioSource, returnDelay);
    }

    public void PlayClipMatchNextBeginning(AudioClip clip)
    {
        var mainAudioSource = CurrentAudioSource;
        var mainClip = CurrentAudioClip;

        var audioSource = _additionalAudioSources.Rent();
        audioSource.clip = clip;

        var clipDelay = mainClip.length - mainAudioSource.time;
        FadeIn(audioSource, 0, clipDelay, false);

        var returnDelay = clipDelay + clip.length;
        ReturnAudioSource(audioSource, returnDelay);
    }

    public void StopClip(AudioClip clip, float fadeOutDuration = 0, bool stopImmediately = false)
    {
        var audioSource = AudioSourceFor(clip);

        var delay = stopImmediately ? 0 : clip.length - audioSource.time - fadeOutDuration;
        FadeOut(audioSource, fadeOutDuration, delay);
    }

    public void StopCurrentClip(float fadeOutDuration = 0, bool stopImmediately = false)
    {
        StopClip(CurrentAudioClip, fadeOutDuration, stopImmediately);
    }

    public void NextClip(float crossFadeDuration = 0)
    {
        var clip0 = CurrentAudioClip;
        var audio0 = CurrentAudioSource;
        var audio1 = NextAudioSource;

        var delay = clip0.length - crossFadeDuration / 2 - audio0.time;
        Crossfade(audio0, audio1, crossFadeDuration, delay);

        MainThreadDispatcher.StartUpdateMicroCoroutine(IncrementClipIndexCoroutine(delay));
    }

    private int IndexOf(AudioClip clip)
    {
        for (var i = 0; i < MusicClips.Length; i++)
            if (MusicClips[i] == clip)
                return i;

        throw new ArgumentException();
    }

    private int IndexOf(AudioSource source)
    {
        for (var i = 0; i < _audioSources.Length; i++)
            if (_audioSources[i] == source)
                return i;

        throw new ArgumentException();
    }

    private AudioSource AudioSourceFor(AudioClip clip)
    {
        return _audioSources[IndexOf(clip)];
    }

    private bool LockAudioSource(AudioSource audioSource)
    {
        var audioIndex = IndexOf(audioSource);

        if (_audioSourceIsBusy[audioIndex])
            return false;

        _audioSourceIsBusy[audioIndex] = true;

        return true;
    }

    private void ReleaseAudioSource(AudioSource audioSource)
    {
        _audioSourceIsBusy[IndexOf(audioSource)] = false;
    }

    private void ReturnAudioSource(AudioSource audioSource, float delay = 0)
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(ReturnAudioSourceCoroutine(audioSource, delay));
    }

    public void Crossfade(AudioSource from, AudioSource to, float duration, float delay = 0, float startTime = 0)
    {
        FadeOut(from, duration, delay);
        FadeIn(to, duration, delay, startTime: startTime);
    }

    public void FadeIn(AudioSource audioSource, float duration, float delay = 0, bool lockAudioSource = true,
        float startTime = 0)
    {
        if (lockAudioSource && !LockAudioSource(audioSource))
        {
            print(string.Format("AudioSource ({0}) is busy.", audioSource.name));
            return;
        }

        MainThreadDispatcher.StartUpdateMicroCoroutine(FadeInCoroutine(audioSource, duration, delay, startTime));

        if (lockAudioSource)
            ReleaseAudioSource(audioSource);
    }

    public void FadeOut(AudioSource audioSource, float duration, float delay = 0, bool stopAudio = false,
        bool lockAudioSource = true)
    {
        if (lockAudioSource && !LockAudioSource(audioSource))
        {
            print(string.Format("AudioSource ({0}) is busy.", audioSource.name));
            return;
        }

        MainThreadDispatcher.StartUpdateMicroCoroutine(FadeOutCoroutine(audioSource, duration, delay, stopAudio));

        if (lockAudioSource)
            ReleaseAudioSource(audioSource);
    }

    public void PitchShift(float target, float duration)
    {
        MainThreadDispatcher.StartUpdateMicroCoroutine(PitchShiftCoroutine(target, duration));
    }

    #region fade coroutines

    private IEnumerator FadeInCoroutine(AudioSource audioSource, float duration, float delay = 0, float startTime = 0)
    {
        float endTime;
        if (delay > 0)
        {
            endTime = Time.time + delay;
            while (Time.time < endTime)
                yield return null;
        }

        if (!audioSource.isPlaying)
        {
            audioSource.volume = 0;
            audioSource.time = startTime;
            audioSource.Play();
        }

        endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            audioSource.volume += Time.deltaTime / duration;
            yield return null;
        }

        audioSource.volume = 1;
    }

    private IEnumerator FadeOutCoroutine(AudioSource audioSource, float duration, float delay = 0,
        bool stopAudio = false)
    {
        float endTime;
        if (delay > 0)
        {
            endTime = Time.time + delay;
            while (Time.time < endTime)
                yield return null;
        }

        endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            audioSource.volume -= Time.deltaTime / duration;
            yield return null;
        }

        audioSource.volume = 0;
        if (stopAudio)
            audioSource.Stop();
    }

    #endregion

    private IEnumerator ReturnAudioSourceCoroutine(AudioSource audioSource, float delay)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
        {
            yield return null;
        }

        _additionalAudioSources.Return(audioSource);
    }

    private IEnumerator IncrementClipIndexCoroutine(float delay = 0)
    {
        var endTime = Time.time + delay;
        while (Time.time < endTime)
        {
            yield return null;
        }
        _currentClipIndex = (_currentClipIndex + 1) % MusicClips.Length;
    }

    private IEnumerator PitchShiftCoroutine(float target, float duration)
    {
        var step = (target - MusicPitch) / duration;
        var endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            MusicPitch += step * Time.deltaTime;
            yield return null;
        }
        MusicPitch = target;
    }
}
